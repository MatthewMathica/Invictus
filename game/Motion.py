from direct.interval.IntervalGlobal import *
from math import pi
from game.IMath import Transformation

# Kinematics effects representing translational, rotational effect affecting NodePath in scene

def move(actor, to, duration):
    def update(actor, to, duration):
        actor.setPos(actor, to * globalClock.getDt())

    moveInterval = Func(update, actor, to, duration)
    return Sequence(moveInterval)

def camera_driven_move(actor, camera,  vector, degree, speed):
    def update(actor, camera, vector, degree, speed):
        alpha = camera.getAlphaDegree() + degree
        direction = Transformation.getVecFromBasis(vector,

                                                   camera.getFront(),
                                                   camera.getRight(),
                                                   camera.getUp()
        )
        actor.setH(alpha)
        actor.setPos(actor.getPos(base.render) + direction * speed * globalClock.getDt())

    moveInterval = Func(update, actor, camera, vector, degree, speed)
    return Sequence(moveInterval)

from direct.fsm.FSM import FSM
from game.IMath import AnimationBlending
from direct.interval.IntervalGlobal import *
from panda3d.core import Vec3
import game.Motion as Motion

class PlayerFSM(FSM):

    # CODEREVIEW Motko 2018-09-17
    # Why use state machine? - use interpolation between unrelated concerns instead
    # Azimuth Direction - W forward, A left, D right, S back - sum direction vectors, normalize
    # Speed - Direction not set => 0, Direction set => default speed, Direction set + shift pressed => sprint speed

    def __init__(self, actor, camera):
        self.actor = actor
        self.actor.enableBlend()
        self.camera = camera

        FSM.__init__(self, 'PlayerFSM')


        self.states = {
            ('Run', 'shift-a') : ('RunLeft', 'run'),
            ('Run', 'shift-d') : ('RunRight','run'),
            ('Run', 'w-up') : ('Walk', 'run'),
            ('Walk', 'a') : ('WalkLeft','walk'),
            ('Walk', 'd') : ('WalkRight', 'walk'),
            ('Walk', 'w-up') : ('Idle', 'walk'),
            ('Walk', 'shift') : ('Run',  'walk'),
            ('Idle', 'w') : ('Walk', 'idle'),
            ('RunLeft', 'lshift-up') : ('Walk', 'run'),
            ('RunRight', 'lshift-up') : ('Walk', 'run')
        }

    def defaultFilter(self, request, args):
        key = (self.state, request)
        print(request)
        return self.states.get(key)

    def setKey(self, key):
        self.request(key)

    def filterIdle(self, request, args):
        if request == 'w':
            return 'Walk', 'idle'
        else:
            return None
    
    def enterIdle(self, args):
        # Blend animation from previous animation
        # For now, without sync
        AnimationBlending.crossBlend(self.actor, 'idle', args, 1.0)
        # Apply kinematics

    def enterWalk(self, args):
        # Blend animation from previous animation
        # For now, without sync
        AnimationBlending.crossBlend(self.actor, 'walk', args, 0.3)
        # Dynamic / Kinematic
        self.kinematics = Motion.camera_driven_move(self.actor,
                                                    self.camera,
                                                    Vec3(1.0, 0.0, 0.0),
                                                    0,
                                                    1.0)
        self.kinematics.loop()

    def enterRun(self, args):
        # Blend animation from previous animation
        # For now, without sync
        AnimationBlending.crossBlend(self.actor, 'run', args, 0.5)
        self.kinematics = Motion.camera_driven_move(self.actor,
                                                    self.camera,
                                                    Vec3(1.0, 0.0, 0.0),
                                                    0,
                                                    3.0)
        self.kinematics.loop()

    def enterRunLeft(self, args):
        # Blend animation from previous animation
        # For now, without sync
        
        self.kinematics = Motion.camera_driven_move(self.actor,
                                                    self.camera,
                                                    Vec3(0.0, -1.0, 0.0),
                                                    90,
                                                    3.0)
        self.kinematics.loop()

    def enterRunRight(self, args):
        # Blend animation from previous animation
        # For now, without sync
       
        self.kinematics = Motion.camera_driven_move(self.actor,
                                                     self.camera,
                                                     Vec3(0.0, 1.0, 0.0),
                                                     -90,
                                                     3.0
        )


        self.kinematics.loop()

    def exitRunRight(self):
        print("Exit Run Right")
        self.kinematics.finish()

    def exitRunLeft(self):
        print("Exit Run left")
        self.kinematics.finish()

    def exitRun(self):
        print("Exit running")
        self.kinematics.finish()

    def exitWalk(self):
        print("Exit walk")
        self.kinematics.finish()

    def exitIdle(self):
        print("Exit idle")

from panda3d.core import Vec3
from Packet  import Packet

# State 
class State:
    
    def __init__(self):
        self.Acceleration = Vec3()
        self.Velocity = Vec3()
        self.Rotation = Vec3()
        self.Location = Vec3()
    
    @classmethod
    def fromPhysx(cls, acc, vel, pos, rot):
        state = cls()
        state.setState(acc, vel, pos, rot)
        return state
    
    @classmethod
    def fromPacket(cls, d):
        state = cls()

        state.setState(Acceleration = d.getVec3(),
                       Velocity = d.getVec3(),
                       Rotation = d.getVec3(),
                       Location = d.getVec3())
        

        return state


    def setState(self, Acceleration, Velocity, Rotation, Location):
        self.Acceleration = Acceleration
        self.Velocity = Velocity
        self.Rotation = Rotation
        self.Location = Location


    def applyTo(self, nodePath):
        nodePath.setPos(self.Location)
        nodePath.setHpr(self.Rotation)
        # TODO : Animation, Physics


    # Appends itself into datagram
    def copyIntoDatagram(self, d):
        return d.appendV3([self.Acceleration,
                         self.Velocity,
                         self.Rotation,
                         self.Location])

    # Operators
    def __add__(self, o):
        return StateTemplate.fromPhysx(self.Acceleration + o.Acceleration,
                                       self.Velocity + o.Velocity,
                                       self.Rotation + o.Rotation,
                                       self.Location + o.Location)
    def __sub__(self, o):
        return StateTemplate.fromPhysx(self.Acceleration - o.Acceleration,
                                       self.Velocity - o.Velocity,
                                       self.Rotation - o.Rotation,
                                       self.Location - o.Location)

    def __mul__(self, n):
        return StateTemplate.fromPhysx(self.Acceleration * n,
                                       self.Velocity * n,
                                       self.Rotation * n,
                                       self.Location * n)
    def __rmul__(self, n):
        return StateTemplate.fromPhysx(self.Acceleration * n,
                                       self.Velocity * n,
                                       self.Rotation * n,
                                       self.Location * n)
    
    def __str__(self):
        return str(self.Location) + " " + str(self.Rotation)

    # Getters
    @property
    def Acceleration(self):
        return self.__Acceleration
    @property
    def Velocity(self):
        return self.__Velocity
    @property
    def Rotation(self):
        return self.__Rotation
    @property
    def Location(self):
        return self.__Location

    # Setters
    @Acceleration.setter
    def Acceleration(self, v):
        self.__Acceleration = v
    @Velocity.setter
    def Velocity(self, v):
        self.__Velocity = v
    @Rotation.setter
    def Rotation(self, v):
        self.__Rotation =v
    @Location.setter
    def Location(self, v):
        self.__Location = v




def __main__():
   
    packet = Packet()
    packet.addVec3(Vec3(1.0))
    packet.addVec3(Vec3(1.0))
    packet.addVec3(Vec3(1.0))
    packet.addVec3(Vec3(2.0))
    packet.lock()

    state = State.fromPacket(packet)
    
    print(state)
__main__()

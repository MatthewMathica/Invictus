from math import pi, cos
from panda3d.core import Quat, Vec3



class Interpolation:
    
    @staticmethod
    def linear(a, b, t):
        return (b - a) * t + a

    @staticmethod
    def cosine(a, b, t):
        s = 0.5 * (1 - cos(t * pi))
        return (b - a) * s + a

    @staticmethod
    def quatSlerp(t, s, t):
        n = s * t * s.conjugate()
        return n


    # animations
    # {animation, bounding part}
    def splitAnimations(animations):
        for i in animations:
            bound = i.bounding
            animation = i.animation




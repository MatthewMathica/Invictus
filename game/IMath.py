from math import pi, cos, sin
from panda3d.core import Quat, Vec3

from direct.interval.IntervalGlobal import *



class KinematicBlending:

    @staticmethod
    def crossBlend(actor, fadeIn, fadeOut, duration):
        pass

class AnimationBlending:

    @staticmethod
    def crossBlend(actor, fadeIn, fadeOut, duration):

        actor.setControlEffect(fadeIn, 0.0)
        actor.loop(fadeIn)

        if fadeOut is not None:
            seq = Sequence(LerpAnimInterval(actor,
                                            duration = duration,
                                            startAnim = fadeOut,
                                            endAnim = fadeIn,
                                            startWeight = 0.0,
                                            endWeight = 1.0),
                           Func(actor.stop, fadeOut)
                           )
            seq.start()
        else:
            actor.setControlEffect(fadeIn, 1.0)

class Angles:

    def fromSphericalVector(vector):
        pass

    @staticmethod
    def toSphericalVector(alpha, delta):
        s = sin(delta)

        xr = s * cos(alpha)
        yr = s * sin(alpha)
        zr = cos(delta)

        v = Vec3(xr, yr, zr)
        v.normalize()

        return v

class Interpolation:

    @staticmethod
    def linear(a, b, t):
        return (b - a) * t + a

    @staticmethod
    def cosine(a, b, t):
        s = 0.5 * (1 - cos(t * pi))
        return (b - a) * s + a

    @staticmethod
    def quatSlerp(t, s):
        n = s * t * s.conjugate()
        return n

class Transformation:

    @staticmethod
    def getVecFromBasis(vec, x, y, z):
        return x * vec.getX() + y * vec.getY() + z * vec.getZ()

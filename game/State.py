from panda3d.core import Vec3, Mat4
from game.Packet  import Packet

# State 
class State:
    
    def __init__(self):
        self.Acceleration = Vec3()
        self.Velocity = Vec3()
        self.Rotation = Vec3()
        self.Location = Vec3()
        self.Animation = ""
        self.cFrame = 0

    def toMatrix(self):
       pass

    @classmethod
    def fromPhysxAnim(cls, acc, vel, rot, pos, animation, frame = 0):
        state = cls()
        state.setState(acc, vel, rot, pos, animation, frame)
        return state

    @classmethod
    def fromPacket(cls, d):
        state = cls()

        state.setState(d.getVec3(),
                       d.getVec3(),
                       d.getVec3(),
                       d.getVec3(),
                       d.getString(),
                       d.getInt32()
        )

        return state


    def setState(self, Acceleration, Velocity, Rotation, Location, Animation, Frame):
        self.Acceleration = Acceleration
        self.Velocity = Velocity
        self.Rotation = Rotation
        self.Location = Location
        self.Animation = Animation
        self.cFrame = Frame

    def applyTo(self, nodePath):
        nodePath.setPos(self.Location)
        nodePath.setHpr(self.Rotation)
        if nodePath.getAnimControl(self.Animation).isPlaying() == False:
            nodePath.loop(self.Animation)
        # TODO : Animation, Physics

    # Appends itself into datagram
    def copyIntoPacket(self, d):
        d.appendV3([self.Acceleration,
                         self.Velocity,
                         self.Rotation,
                         self.Location])
        d.addString(self.Animation)
        d.addInt32(self.cFrame)
    
    # Interpolation
    def interpolate(self, state, func, t):
        pass

    # Operators
    def __add__(self, o):
        return StateTemplate.fromPhysx(self.Acceleration + o.Acceleration,
                                       self.Velocity + o.Velocity,
                                       self.Rotation + o.Rotation,
                                       self.Location + o.Location)
    def __sub__(self, o):
        return StateTemplate.fromPhysx(self.Acceleration - o.Acceleration,
                                       self.Velocity - o.Velocity,
                                       self.Rotation - o.Rotation,
                                       self.Location - o.Location)

    def __mul__(self, n):
        return StateTemplate.fromPhysx(self.Acceleration * n,
                                       self.Velocity * n,
                                       self.Rotation * n,
                                       self.Location * n)
    def __rmul__(self, n):
        return StateTemplate.fromPhysx(self.Acceleration * n,
                                       self.Velocity * n,
                                       self.Rotation * n,
                                       self.Location * n)
    
    def __str__(self):
        return str(self.Location) + " " + str(self.Rotation) + " " + self.Animation

  

    # Getters
    @property
    def Acceleration(self):
        return self.__Acceleration
    @property
    def Velocity(self):
        return self.__Velocity
    @property
    def Rotation(self):
        return self.__Rotation
    @property
    def Location(self):
        return self.__Location
    @property
    def Animation(self):
        return self.__Animation

    # Setters
    @Acceleration.setter
    def Acceleration(self, v):
        self.__Acceleration = v
    @Velocity.setter
    def Velocity(self, v):
        self.__Velocity = v
    @Rotation.setter
    def Rotation(self, v):
        self.__Rotation =v
    @Location.setter
    def Location(self, v):
        self.__Location = v
    @Animation.setter
    def Animation(self, v):
        self.__Animation = v
    


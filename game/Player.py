from game.State import State
from panda3d.core import Vec3
class Player:

    def __init__(self):
        self.State = None
        self.Character = None
        self.Id = None
        self.prevAnimation = None

    @classmethod
    def fromState(cls, player_id, state, character):
        player = cls()
        player.Id = player_id
        player.State = state
        player.Character = character

        return player

    def copyIntoPacket(self, p):

        p.addByte(2)
        p.addInt32(self.Id)
        State.fromPhysxAnim(Vec3(0, 0, 0), Vec3(0, 0, 0), self.Character.getHpr(), self.Character.getPos(), self.State.Animation, 0).copyIntoPacket(p)

    def getActiveAnimation(self):
        animations = self.Character.getAnimNames();
        for animation in animations:
            print(animation.__str__())
            control = self.Character.getAnimControl(animation)
            if control.isPlaying():
                print(animation)
                return animation.__str__()

    @property
    def Character(self):
        return self.__Character

    @Character.setter
    def Character(self, v):
        self.__Character = v

    @property
    def Id(self):
        return self.__Id

    @Id.setter
    def Id(self, v):
        self.__Id = v

    @property
    def State(self):
        return self.__State

    @State.setter
    def State(self, val):
        self.__State = val





from panda3d.core import QueuedConnectionManager
from panda3d.core import QueuedConnectionReader
from panda3d.core import ConnectionWriter
from direct.task import Task
from direct.distributed.PyDatagram import PyDatagram
from direct.distributed.PyDatagramIterator import PyDatagramIterator
from panda3d.core import NetDatagram
from panda3d.core import Vec3
from game.StateManager import StateManager
from game.State import State
from game.Handler import GameHandler
from game.Packet import Packet
# Theese should be defined by external module (config, discovery servers)
class iClient():
    
    def __init__(self, player, scene):

        # Network is delegating packets into GameHandler
        self.gameHandler = GameHandler(None, player, scene)
        self.player = player
        port_address = 6066
        ip_address = "127.0.0.1"
        timeout_milis = 1000

        self.cManager = QueuedConnectionManager()
        self.cReader = QueuedConnectionReader(self.cManager, 0)
        self.cWriter = ConnectionWriter(self.cManager, 0)

        self.connection = self.cManager.openTCPClientConnection(ip_address,
                                                                port_address,
                                                                timeout_milis)
        if self.connection:
            self.cReader.addConnection(self.connection)
            taskMgr.add(self.tskReaderPolling, "Polling reader data", -1)

    def tskReaderPolling(self, data):

        for i in range(1,100):
            if self.cReader.dataAvailable():
                datagram = NetDatagram()

                if self.cReader.getData(datagram):
                    self.processingFunction(datagram)
            else:
                break

        return Task.cont

    def tskPlayerPolling(self):

        packet = Packet()
        self.player.copyIntoPacket(packet)
        self.sendDatagram(packet)


    def sendDatagram(self,packet):
        self.cWriter.send(packet.Datagram , self.connection) 


    def processingFunction(self, datagram):
        packet = Packet.fromNetDatagram(datagram)
        self.gameHandler.handle(packet)





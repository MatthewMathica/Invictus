from game.State import State
from panda3d.core import Vec3, Vec2
from math import sin, cos, pi
from game.IMath import Angles, Interpolation
# Double buffered update
# Panda camera, Virtual camera
class CameraWrap:

    eps = 0.12
    magic = 25.0
    
    def __init__(self, camera):
        #state
        self.state = State()
        self.radius = 5
        self.alpha = 0.0
        self.delta = 0.0
        self.target = None
        self.position = Vec3(0,0,10)
        self.pandaCamera = camera
        self.front = Vec3()
        self.right = Vec3()
        self.up = Vec3(0.0,0.0,1.0)
        self.velocity = Vec2(0.0)

    @classmethod
    def fromConfig(cls,camera, conf):
        camera = cls(camera)
        camera.config(conf["sensitivityX"],
                      conf["sensitivityY"])
        return camera

    def config(self, sensitivityX = 2.0, sensitivityY = 2.0):
        self.sensitivityX = sensitivityX
        self.sensitivityY = sensitivityY

    def lookAt(self, node):
        newFront = node.getPos()


    def setRelativePosition(self, v):
        self.position = v

    def targetTo(self, node):
        self.target = node
        
    def sphericalControls(self, x, y, dt):

        self.velocity = self.velocity + (Vec2(x, y) - self.velocity) * dt * self.magic

        self.alpha = self.alpha + self.velocity.getX() * self.sensitivityX

        #We need limit delta because of mirroring
        self.delta = min(pi , max(self.delta - self.velocity.getY() * self.sensitivityY, self.eps))

        spherical = Angles.toSphericalVector(self.alpha, self.delta)

        self.front = -spherical
        self.front.setZ(0)
        self.front.normalize()

        self.right = self.front.cross(self.up)
        self.right.normalize()

        return spherical * self.radius + self.target.getPos(base.render)

    def getAlphaDegree(self):
        return (180 * (self.alpha - 0.5*pi)) / pi

    def getAlpha(self):
        return self.alpha

    def getDelta(self):
        return self.delta

    def getUp(self):
        return self.up

    def getRight(self):
        return self.right

    def getFront(self):
        return self.front

    def zoom(self, delta):
        pass

    def applyToScene(self, scene):
        pass

    # Storing ref to panda camera
    def enable(self, pandaCamera):
        self.pandaCamera = pandaCamera

    # Remove ref to panda camera
    def disable(self):
        self.pandaCamera = None

    # Centerized mouse coordinates
    def update(self, x, y, dt):

        translate = self.sphericalControls(x, y, dt) 
        
        self.pandaCamera.setPos(translate)
        self.pandaCamera.lookAt(self.target)



from panda3d.core import Vec3
from direct.distributed.PyDatagram import PyDatagram
from panda3d.core import NetDatagram
from direct.distributed.PyDatagramIterator import PyDatagramIterator

# Packet represents wrapper around RAW datagram
class Packet:
    
    def __init__(self):
        self.Datagram = PyDatagram()

    
    def lock(self):
        self.Iterator = PyDatagramIterator(self.Datagram)


    @classmethod
    def fromNetDatagram(cls, netDatagram):
        packet = Packet()
        packet.Iterator = PyDatagramIterator(netDatagram)
        return packet

    @classmethod
    def fromState(cls, state):
        packet = Packet()
        state.copyIntoDatagram(packet)
        return packet
    
    def addVec3(self, v):
        self.Datagram.addFloat64(v.getX())
        self.Datagram.addFloat64(v.getY())
        self.Datagram.addFloat64(v.getZ())

    def getVec3(self):
        return Vec3(self.Iterator.getFloat64(),
                    self.Iterator.getFloat64(),
                    self.Iterator.getFloat64())

    def addByte(self, v):
        self.Datagram.addUint8(v)

    def addString(self, v):
        self.Datagram.addString(v)

    def addInt32(self, v):
        self.Datagram.addInt32(v)

    def addInt64(self, v):
        self.Datagram.addInt64()

    def getInt64(self):
        return self.Iterator.getInt64()

    def getInt32(self):
        return self.Iterator.getInt32()

    def getFloat64(self):
        return self.Iterator.getFloat64()

    def getByte(self):
        return self.Iterator.getUint8()
    def getString(self):
        return self.Iterator.getString();

    def appendV3(self, array):
        for i in array:
            self.addVec3(i)
            

    def __str__(self):
        return str(self.Datagram)

    

    @property
    def Iterator(self):
        return self.__Iterator

    @Iterator.setter
    def Iterator(self, v):
        self.__Iterator = v 

    @property
    def Datagram(self):
        return self.__Datagram

    @Datagram.setter
    def Datagram(self, v):
        self.__Datagram = v



from game.Controls import Controls

class PlayerControls(Controls):
    
    def __init__(self, player, camera, abilities):
        self.camera = camera
        self.player = player
        self.abilities = abilities
        
    def handleMouse(self, mouse, dt):
        if self.camera != None:
            self.camera.update(mouse[0], mouse[1], dt)

    def handleKeys(self, key):
        self.player.Character.getPythonTag("state").interrupt(self.player.Character, key)
        
    def getCamera(self):
        return self.camera

    def setCamera(self, camera):
        self.camera = camera

    def setAbilities(self, abilities):
        self.abilities = abilities

    def getAbilities(self):
        return self.abilities

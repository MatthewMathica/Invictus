from panda3d.core import QueuedConnectionManager
from panda3d.core import QueuedConnectionListener
from panda3d.core import QueuedConnectionReader
from panda3d.core import ConnectionWriter
from panda3d.core import PointerToConnection
from panda3d.core import NetAddress
from panda3d.core import NetDatagram
from direct.distributed.PyDatagram import PyDatagram
from direct.distributed.PyDatagramIterator import PyDatagramIterator
from direct.task import Task
from direct.showbase.ShowBase import ShowBase



# Theese should be defined by external module (config, constants)

#Invictus Server
class iServer(ShowBase):

    def __init__(self):
        ShowBase.__init__(self)
        # QueuedConnectionManager
        # - handles low level connection processes
        self.cManager = QueuedConnectionManager()

        # QueuedConnectionListener
        # - waits for clients to request connection
        self.cListener = QueuedConnectionListener(self.cManager, 0)

        #QueuedConnectionReader
        # - buffers incoming data from active connection
        self.cReader = QueuedConnectionReader(self.cManager, 0)

        #ConnectionWriter
        # - allows PyDatagram to be trasmitted out along an active connection
        self.cWriter = ConnectionWriter(self.cManager, 0)

        # Array of active connections
        self.activeConnections = []

        # TCP - port for incoming connections
        # backlog - #rejected_incoming_request

        port_address = 6066
        backlog = 2
        tcpSocket = self.cManager.openTCPServerRendezvous(port_address, backlog)

        self.cListener.addConnection(tcpSocket)


        # Polling incoming connections, requests
        print("Creating server")
        self.taskMgr.add(self.tskListenerPolling, "tskListenerPolling")
        self.taskMgr.add(self.tskReaderPolling, "tskReaderPolling")


    # Polling new connection manager
    def tskListenerPolling(self, taskData):
        if self.cListener.newConnectionAvailable():
            rendezvous = PointerToConnection()
            netAddress = NetAddress()
            newConnection = PointerToConnection()

            if self.cListener.getNewConnection(rendezvous, netAddress, newConnection):
                newConnection = newConnection.p()
                self.activeConnections.append(newConnection)
                self.cReader.addConnection(newConnection)
                
        return Task.cont

    # Polling new incoming data
    def tskReaderPolling(self, taskData):
        for i in range(1,100):
            if self.cReader.dataAvailable():
                datagram = NetDatagram()

                if self.cReader.getData(datagram):
                    self.processFunction(datagram)
            else:
                break
        return Task.cont



    # Broadcasting (temporary)
    def processFunction(self, datagram):
        source = datagram.getConnection()
        
        for i in self.activeConnections:
            if i != source:
                self.cWriter.send(datagram, i)

app = iServer()
app.run()



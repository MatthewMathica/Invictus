#!/usr/bin/env python

# Author: Ryan Myers
# Models: Jeff Styers, Reagan Heller
#
# Last Updated: 2015-03-13
#
# This tutorial provides an example of creating a character
# and having it walk around on uneven terrain, as well
# as implementing a fully rotatable camera.
from pandac.PandaModules import WindowProperties
from direct.showbase.ShowBase import ShowBase
from panda3d.core import CollisionTraverser, CollisionNode
from panda3d.core import CollisionHandlerQueue, CollisionRay
from panda3d.core import Filename, AmbientLight, DirectionalLight
from panda3d.core import PandaNode, NodePath, Camera, TextNode
from panda3d.core import CollideMask, Vec3
from panda3d.core import WindowProperties
from direct.gui.OnscreenText import OnscreenText
from direct.actor.Actor import Actor
from panda3d.physics import ActorNode
from game.Camera import CameraWrap
import random
import sys
import os
import math
from abilities.state_builder import create_ability
from game.Player import Player
from game.State import State
from game.Scene import Scene
from game.client_module import iClient as NetworkManager
from game.PlayerControls import PlayerControls
from game.FSM import PlayerFSM
from game.IMath import AnimationBlending
from direct.interval.IntervalGlobal import *
from panda3d.physics import ForceNode, LinearVectorForce

# Function to put instructions on the screen.
def addInstructions(pos, msg):
    return OnscreenText(text=msg, style=1, fg=(1, 1, 1, 1), scale=.05,
                        shadow=(0, 0, 0, 1), parent=base.a2dTopLeft,
                        pos=(0.08, -pos - 0.04), align=TextNode.ALeft)

# Function to put title on the screen.
def addTitle(text):
    return OnscreenText(text=text, style=1, fg=(1, 1, 1, 1), scale=.07,
                        parent=base.a2dBottomRight, align=TextNode.ARight,
                        pos=(-0.1, 0.09), shadow=(0, 0, 0, 1))


class Invictus(ShowBase):
    Multiplayer = False

    def __init__(self):
        # Set up the window, camera, etc.
        ShowBase.__init__(self)
        props = WindowProperties()
        props.setTitle('Invictus - Dev')
        base.win.requestProperties(props)
        # Enable physics
       



        self.ralph = Actor("./models/little_warrior",
                                {"walk": "./models/animations/walk",
                                 "run":"./models/animations/run","die":"models/animations/dance1",
                                 "idle":"./models/animations/idle",
                                 "slash":"./models/animations/slash",
                                 "slash2":"./models/animations/slash2",
                                "lpunch":"./models/animations/lpunch",
                                 "rpunch":"./models/animations/rpunch"})

      	
        

        # Player character
        self.Player = Player.fromState(int(sys.argv[1]) , State.fromPhysxAnim(
            Vec3(0.0, 0.0, 0.0),
            Vec3(0.0, 0.0, 0.0),
            Vec3(0.0, 0.0, 0.0),
            Vec3(0.0, 0.0, 0.0),
            "idle",
            0
        ), self.ralph)
        self.ralph.reparentTo(render)
        self.actCamera = CameraWrap.fromConfig(self.camera , {"sensitivityX" : 2.0,
                                                              "sensitivityY" : 2.0
        })

        self.floater = NodePath(PandaNode("floater"))
        self.floater.reparentTo(self.ralph)
        self.floater.setZ(2.0)

        self.actCamera.targetTo(self.floater)
        self.actCamera.update(0.0, 0.0, 0.0)


        self.fsm = PlayerFSM(self.ralph, self.actCamera)
        self.fsm.request('Idle', None)


        #Scene
        self.scene = Scene()
        self.scene.nodes[self.Player.Id] = self.Player
        # Set Multiplayer
        # self.Multiplayer = NetworkManager(self.Player, self.scene)

        # Set the background color to black
        self.win.setClearColor((1, 1, 1, 1))

        # This is used to store which keys are currently pressed.
        self.keyMap = {
            "a": 0, "d": 0, "w": 0, "cam-left": 0, "cam-right": 0,"dance":0, "zoom": 0, "unzoom": 0}

        # Post the instructions
        self.title = addTitle(
            "Invictus")

        self.environ = loader.loadModel("maps/demo_map")
        self.environ.reparentTo(render)

       
        self.ralph.setScale(0.5)
        self.ralph.setPos((0, 0, 0.5))

        base.buttonThrowers[0].node().setButtonDownEvent('downButton')
        base.buttonThrowers[0].node().setButtonUpEvent('upButton')

        doNothing = {
            'name': 'Do nothing',
            'states': [
                {
                    'name': 'stand',
                    'duration': 2.0,
                    'initial': [
                        {
                            'type' : 'PLAY_ANIMATION',
                            'actor' : 'ACTOR',
                            'animation' : 'idle',
                            'duration' : 2.0
                        }
                    ],
                    'effects': [
                        {
                            'type': 'SET_OUT_STATE',
                            'state': 'stand'
                        }
                    ],
                    'interrupts': [
                        {
                            'name' : 'Run_forward',
                            'trigger' : 'w-down',
                            'effects' : [
                                {
                                    'type' : 'SET_OUT_STATE',
                                    'state' : 'move_forward'
                                }
                            ]
                        }
                    ]
                },
                {
                    'name' : 'move_forward',
                    'duration' : 1.0,
                    'initial' : [
                        {
                            'type': 'PLAY_ANIMATION',
                            'actor' : 'ACTOR',
                            'duration' : 1.0,
                            'animation' : 'run'
                        },
                        {
                            'type' : 'MOVE',
                            'actor' : 'ACTOR',
                            'duration' : 1.0,
                            'delta' : [-1, 0, 0]
                        }

                    ],
                    'effects' : [
                        {
                            'type' : 'SET_OUT_STATE',
                            'state' : 'move_forward'
                        }

                    ],
                    'interrupts' : [
                        {
                            'name' : 'SlowDown',
                            'trigger' : 'w-up',
                            'effects':[
                                {
                                    'type' : 'SET_OUT_STATE',
                                    'state' : 'stand'
                                }
                            ]
                        }
                    ]
                }
            ]
        }


        self.accept("downButton", self.downButton)
        self.accept("upButton", self.upButton)

        self.PlayerControls = PlayerControls(self.Player, self.actCamera, {})


        taskMgr.add(self.move, "moveTask")


        # Game state variables
        self.isMoving = False

        # Set up the camera
        self.disableMouse()

        props = WindowProperties()
        props.setCursorHidden(True)
        props.setMouseMode(WindowProperties.M_relative)
        base.win.requestProperties(props)



        self.cTrav = CollisionTraverser()

        self.ralphGroundRay = CollisionRay()
        self.ralphGroundRay.setOrigin(0, 0, 99)
        self.ralphGroundRay.setDirection(0, 0, -1)
        self.ralphGroundCol = CollisionNode('ralphRay')
        self.ralphGroundCol.addSolid(self.ralphGroundRay)
        self.ralphGroundCol.setFromCollideMask(CollideMask.bit(0))
        self.ralphGroundCol.setIntoCollideMask(CollideMask.allOff())
        self.ralphGroundColNp = self.ralph.attachNewNode(self.ralphGroundCol)
        self.ralphGroundHandler = CollisionHandlerQueue()
        self.cTrav.addCollider(self.ralphGroundColNp, self.ralphGroundHandler)

        self.camGroundRay = CollisionRay()
        self.camGroundRay.setOrigin(0, 0, 99)
        self.camGroundRay.setDirection(0, 0, -1)
        self.camGroundCol = CollisionNode('camRay')
        self.camGroundCol.addSolid(self.camGroundRay)
        self.camGroundCol.setFromCollideMask(CollideMask.bit(0))
        self.camGroundCol.setIntoCollideMask(CollideMask.allOff())
        self.camGroundColNp = self.camera.attachNewNode(self.camGroundCol)
        self.camGroundHandler = CollisionHandlerQueue()
        self.cTrav.addCollider(self.camGroundColNp, self.camGroundHandler)


        self.ralphGroundColNp.show()

        ambientLight = AmbientLight("ambientLight")
        ambientLight.setColor((.3, .3, .3, 1))
        directionalLight = DirectionalLight("directionalLight")
        directionalLight.setDirection((-5, -5, -5))
        directionalLight.setColor((1, 1, 1, 1))
        directionalLight.setSpecularColor((1, 1, 1, 1))
        render.setLight(render.attachNewNode(ambientLight))
        render.setLight(render.attachNewNode(directionalLight))

        self.recenterMouseTask = taskMgr.add(self.recenterMouseTask, "Mouse Task", priority = 1)

    def upButton(self, key):
        self.fsm.setKey(key+"-up")

    def downButton(self, key):
        self.fsm.setKey(key)

    def recenterMouseTask(self, task):

        # only execute when the mouse is in the frame
        if base.mouseWatcherNode.hasMouse():

            dt = globalClock.getDt()

            x = base.mouseWatcherNode.getMouseX()
            y = base.mouseWatcherNode.getMouseY()

            self.PlayerControls.handleMouse([x, y], dt)

            base.win.movePointer(0,
              int(base.win.getProperties().getXSize() / 2),
              int(base.win.getProperties().getYSize() / 2))

        return task.cont

    def move(self, task):

        dt = globalClock.getDt()


        startpos = self.ralph.getPos()

        entries = list(self.ralphGroundHandler.getEntries())
        entries.sort(key=lambda x: x.getSurfacePoint(render).getZ())

        if len(entries) > 0 and "Plane" in entries[0].getIntoNode().getName():
            minZ = entries[0].getSurfacePoint(render).getZ()
            self.ralph.setZ(minZ)
        else:
            self.ralph.setPos(startpos)

        # Keep the camera at one foot above the terrain,
        # or two feet above ralph, whichever is greater.

        entries = list(self.camGroundHandler.getEntries())
        entries.sort(key=lambda x: x.getSurfacePoint(render).getZ())

        return task.cont


app = Invictus()
app.run()

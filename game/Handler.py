from game.Player import Player
from game.Scene import Scene
from game.State import State
from direct.actor.Actor import Actor

def decorator_handler(func):
    def wrap(packet):
      return func(packet)

class Handler():

    def handle(self, packet):
        pass

class GameHandler(Handler):

    def __init__(self, game, player, scene):
        self.handleRules = {
            1 : PlayerHandler(player),
            2 : SceneHandler(scene)
        }

    def handle(self, packet):
        packetType = packet.getByte()
        self.handleRules[packetType].handle(packet)

class PlayerHandler(Handler):

    def __init__(self, player):
        self.player = player

    def handle(self, packet):
        state = State.fromPacket(packet)
        self.player.State = state

class SceneHandler(Handler):

    def __init__(self, scene):
        self.scene = scene
        self.nodeHandler = NodeHandler(self.scene.nodes)
        
    def handle(self, packet):
        nodeId = packet.getInt32()

        if nodeId not in self.scene.nodes:
            self.scene.nodes[nodeId] = Actor("./models/little_warrior",
                                {"walk": "./models/animations/walk",
                                 "run":"./models/animations/run","die":"models/animations/dance1",
                                 "idle":"./models/animations/idle",
                                 "slash":"./models/animations/slash",
                                 "slash2":"./models/animations/lpunch",
                                "lpunch":"./models/animations/lpunch",
                                 "rpunch":"./models/animations/rpunch"})
            self.scene.nodes[nodeId].reparentTo(render)

        else:
            node = self.scene.nodes[nodeId]
            state = State.fromPacket(packet)
            state.applyTo(node)
           
        #self.nodeHandler.handle(packet)

class NodeHandler(Handler):

    def __init__(self, nodes):
        self.nodes = nodes
        
    def handle(self, packet):
        #type = packet.getByte()
        nodeId = packet.getInt32()

        state = State.fromPacket(packet)
        state.applyTo(self.nodes[nodeId])

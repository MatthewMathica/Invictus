from direct.interval.LerpInterval import LerpPosInterval, LerpHprInterval
from panda3d.core import Vec3
from direct.interval.IntervalGlobal import *

def initialize_effect_creators(ability_raw, states):
    def create_out_state_effect(effect_raw):
        def set_out_state(state, paramMap):
            paramMap['OUT_STATE'] = state

        try:
            state = states[effect_raw['state']]

        except KeyError:
            raise Exception('State ' + effect_raw['state'] + ' not found for effect of type SET_OUT_STATE in ability ' + ability_raw['name'])

        return lambda paramMap: set_out_state(state, paramMap)

    def create_move_effect(effect_raw):
        def move(actor, duration, delta, paramMap):
            
            return LerpPosInterval(paramMap[actor],
                                   duration = duration,
                                   pos = paramMap[actor].getPos() + Vec3(delta[0], delta[1], delta[2]),
                                   blendType = 'easeIn',
                                   other = None
            )

        try:
            actor = effect_raw['actor']
        except KeyError:
            raise Exception('Actor not specified for effect of type MOVE in ability ' + ability_raw['name'])

        try:
            delta = effect_raw['delta']
        except KeyError:
            raise Exception('Delta not specified for effect of type MOVE in ability ' + ability_raw['name'])

        # Duration is on default = 1.0
        duration = 1.0

        if 'duration' in effect_raw:
            duration = effect_raw['duration']

        return lambda paramMap: move(actor, duration, delta, paramMap)

    def create_rotation_effect(effect_raw):
        def rotate(actor, duration, delta, paramMap):

           return LerpHprInterval(paramMap[actor],
                                  duration = duration,
                                  hpr = paramMap[actor].getHpr() + Vec3(delta[0],delta[1], delta[2])
           )

        try:
            actor = effect_raw['actor']
        except KeyError:
            raise Exception('Actor not specified for effect of type ROTATE in ability' + ability_raw['name'])

        try:
            delta = effect_raw['delta']
        except KeyError:
            raise Exception('Relative rotation Delta not specified for effect of type ROTATE in ability' + ability_raw['name'])

        duration = 1.0
        if 'duration' in effect_raw:
            duration = effect_raw['duration']

        return lambda paramMap : rotate(actor, duration, delta, paramMap)

    def create_play_animation_effect(effect_raw):
        def play_animation(actor, duration, animation, paramMap):

            return paramMap[actor].actorInterval(
                animation,
                loop = 1,
                duration = duration
            )
        try:
            actor = effect_raw['actor']
        except KeyError:
            raise Exception('Actor not specified for effect of type MOVE in ability ' + ability_raw['name'])

        try:
            animation = effect_raw['animation']
        except KeyError:
            raise Exception('Animation not specified for effect of type MOVE in ability ' + ability_raw['name'])

        duration = 1.0
        if 'duration' in effect_raw:
            duration = effect_raw['duration']

        return lambda paramMap: play_animation(actor, duration, animation, paramMap)
    
    effect_creators = {
        'SET_OUT_STATE': create_out_state_effect,
        'MOVE': create_move_effect,
        'ROTATE': create_rotation_effect,
        'PLAY_ANIMATION': create_play_animation_effect
    }

    return effect_creators

from direct.task import Task
from direct.interval.IntervalGlobal import *

class Interrupt:
    name = "Default interrupt"
    trigger = None # the triggering interrupt signal
    effects = [] # the effects that immediately happen when interrupted

    def execute(self, actor):
        paramDict = {
            'ACTOR': actor
        }

        for effect in self.effects:
            effect(paramDict)

        # TODO convert catch to if
        try:
            out_state = paramDict["OUT_STATE"]
            
            if type(out_state) is not State:
                raise Exception("Following state is invalid")
            out_state.prepare(actor)
        except KeyError:
            pass


class State:
    name = "Default state" # the state name
    duration = 0 # the state duration
    initial = [] # the effects that happen when the state begins
    effects = [] # the effects that happen when the state finalizes
    interrupts = [] # the interrupts that can happen when the state receives an interrupt signal
    sequence = Sequence()
    
    def __init__(self):
        pass

    def prepare(self, actor):
        actor.setPythonTag("state", self)

        paramDict = {
            'ACTOR': actor
        }

        parallel = Parallel()
        for effect in self.initial:
            parallel.append(effect(paramDict))

        self.sequence = Sequence(parallel, Func(self.finalize, actor))
        self.sequence.start()

    def finalize(self, actor):
        paramDict = {
            'ACTOR': actor
        }


        print(self.name)

        self.sequence.clearToInitial()

        if actor.getPythonTag("state") == self:
            for effect in self.effects:
                effect(paramDict)
        else:
            return
 
        # TODO convert catches to ifs
        try:
            out_state = paramDict["OUT_STATE"]
        except KeyError:
            raise Exception("No following state set")

        if type(out_state) is not State:
            raise Exception("No following state set")

        out_state.prepare(actor)

    # Interrupt name is key (for now)
    def interrupt(self, actor, interruptName):
        for interrupt in self.interrupts:
            if interrupt.trigger == interruptName:
                self.sequence.clearToInitial()
                interrupt.execute(actor)
                break


import json
from abilities.state import State, Interrupt
from abilities.effects import initialize_effect_creators

def create_ability(ability_raw):
    
    def create_state(state_raw):
        state = State()
        state.name = state_raw['name']
        state.duration = state_raw['duration']
        state.effects = state_raw['effects'] # effects are still in raw data form
        state.interrupts = state_raw['interrupts'] # interrupts are still in raw data form
        state.initial = state_raw['initial'] # initial effects are still in raw data form
        return state
    
    states = {} # TODO replace with getting globally available states

    for state in map(create_state, ability_raw['states']):
        states[state.name] = state

    effect_creators = initialize_effect_creators(ability_raw, states)

    def create_effect(effect_raw):
        try:
            print(effect_raw["type"])
            creator = effect_creators[effect_raw['type']]
        except KeyError:
            raise Exception('Effect type ' + effect_raw['type'] + ' not recognized in ability ' + ability_raw['name'])
        
        return creator(effect_raw)
    
    # process the raw effects
    for state_name, state in states.items():
        state.effects = list(map(create_effect, state.effects))

        state.initial = list(map(create_effect, state.initial))

    def create_interrupt(interrupt_raw):
        interrupt = Interrupt()
        interrupt.name = interrupt_raw['name']
        interrupt.trigger = interrupt_raw['trigger']
        interrupt.effects = list(map(create_effect, interrupt_raw['effects']))
        return interrupt

    for state_name, state in states.items():
        state.interrupts = list(map(create_interrupt, state.interrupts))

    # TODO temp hack
    return states


# Invictus
![alt text](http://download.cyanide-studio.com/images-bb1/logos/Logo_Dwarf_09.png)
## Combat
- Finite State Machine
- Combos along sequence of chars predicate chain

## Server
- Collision Detection
- Damage calculation
- Broadcasting changes

## Character
- Skyrim based (no classes)
- Skill tree
- Life tree

### Stats
- HP 
- ![alt text](https://media.indiedb.com/cache/images/games/1/53/52055/thumb_620x2000/Health_bar_1.gif)
- INT
- CS (Casting speed)
- STR
- AGI
- PRC (Perception) 

### Models
- Base character (dummy)
- Equip covering base character (hair, shield, armor, skins)

### Animations
- Sources : Mixamo.com
- Blending animation
- Cutting animations


### Renderer Tutorial
Download the renderer from git or clone it. You will need to dowload panda sdk python 3.6. From the open the panda3d folder where you installed it do not use pip to install panda3
it works then in limited mode. Install from the python in panda3d folder. After that you will need PyQt5 install that also with the pip from the panda folder.
Then run it with the panda3d python I suggest changing the OS path for that I tried orther methods did not work.
-Link to the renderer: https://github.com/tobspr/RenderPipeline
>>>>>>> README.md
